function enviar(){

    let  run = document.getElementById("txt-run").value;
    let  dv = document.getElementById("txt-dv").value;
    let  nombres = document.getElementById("txt-nombres").value;
    let  apellidoPaterno = document.getElementById("txt-apellido-paterno").value;
    let  apellidoMaterno = document.getElementById("txt-apellido-materno").value;
    let  email = document.getElementById("txt-email").value;
    let  telefono = document.getElementById("txt-telefono").value;
    
    if(isEmpty(run) && isEmpty(dv) && isEmpty(nombres) && 
    isEmpty(apellidoPaterno) && isEmpty(apellidoMaterno) &&
    isEmpty(email) && isEmpty(telefono)
    ){
        console.log('formulario lleno');
        let etiqueta =  document.getElementById("txt-message");
        console.log(etiqueta);
        etiqueta.innerHTML = '<div style="width: 50%; margin: 0 auto;" class="alert alert-success alert-dismissible fade show" role="alert">'+
                'Registro realizado correctamente' + '</div>';
        document.getElementById('frm-usuario').submit();
    }else{
        console.log('formulario con campos vacios');
        let etiqueta =  document.getElementById("txt-message");
        etiqueta.innerHTML = '<div style="width: 50%; margin: 0 auto;" class="alert alert-danger alert-dismissible fade show" role="alert">'+
                'Para registrarse debe ingresar todos los datos!!' + '</div>';       
    }
}

function isEmpty(element){
    if(element !== undefined && element !== '' ){
        return true;
    }else{
        return false;
    }
}