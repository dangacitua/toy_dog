"""toy_dog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import imp
from django.contrib import admin
from django.urls import path
from toy_dog.views.home import p_inicial
from toy_dog.views.galeria import galeria_masc
from toy_dog.views.f_contacto import frm_contactos, env_contacto
from toy_dog.views.f_usuario import  frm_usuario, env_usuario
from toy_dog.views.mantenedor_contacto import load_contacto
from toy_dog.views.mantenedor_usuario import base_usuario
from toy_dog.views import login
from toy_dog.views import logout
from django.contrib.auth.models import Permission, ContentType
from toy_dog.models import Contacto
from toy_dog.views import errorpage
from toy_dog.api.v1 import apicontacto

admin.site.register(Permission)
admin.site.register(ContentType)
admin.site.register(Contacto)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('index/', p_inicial),
    path('', p_inicial),
    path('galeria_2/', galeria_masc),
    path('formulario/', frm_contactos),
    path('contacto/formulario', env_contacto),
    path('form_usuario/', frm_usuario),
    path('usuario/formulario', env_usuario),
    path('mantenedor-contacto/',load_contacto),
    path('mantenedor-usuario/',base_usuario),
    path('login/', login.load),
    path('logout/', logout.logout_user),
    path('error-401', errorpage.error_401_page),
    path('error-403', errorpage.error_403_page),
    # definiremos la API REST    
    path('api/v1/contacto', apicontacto.contacto)#incompleta 
    
]
