CREATE TABLE contacto(
    codigo BIGSERIAL,
    run NUMERIC(13),
    dv varchar(1),
    nombres varchar(80),
    apellido_paterno varchar(80),
    apellido_materno varchar(80),
    email varchar(80),
    telefono varchar(16),
    asunto varchar(250),
    primary key (codigo)
    
);

CREATE TABLE usuario(
    codigo_2 BIGSERIAL,
    run NUMERIC(13),
    dv varchar(1),
    nombres varchar(80),
    apellido_paterno varchar(80),
    apellido_materno varchar(80),
    email varchar(80),
    telefono varchar(16),
    primary key (codigo_2)
    
);