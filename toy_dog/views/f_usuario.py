from django.shortcuts import render
from toy_dog.models import Usuario

def frm_usuario(request):
    print('form_usuario')
    return render(request, 'form_usuario.html') 

def env_usuario(request):
    print('formulario_usuario')
    
    if request.method == 'GET':
        print('invocación por método GET')
        run = request.GET.get('run')
        print('run {0}'.format(run))

    elif request.method == 'POST':
        print('invocación por método POST')
        
        run =                       request.POST.get('run')
        dv =                        request.POST.get('dv')
        nombres =                   request.POST.get('nombres')
        apellido_paterno =          request.POST.get('apellido-paterno')
        apellido_materno =          request.POST.get('apellido-materno')
        email =                     request.POST.get('email')
        telefono =                  request.POST.get('telefono') 
        #Crear un objeto Contacto que posee relacion con la tabla contacto          
        usuario = Usuario()
        usuario.run =              run
        usuario.dv =               dv
        usuario.nombres =          nombres
        usuario.apellido_paterno = apellido_paterno
        usuario.apellido_materno = apellido_materno
        usuario.email =            email
        usuario.telefono =         telefono
        usuario.save()
         # contacto.save guarda
         # contacto.delete borra
         # contacto.save(force_update)
         # contacto.objects.all muestra todos los datos de la tabla
        print('run {0}'.format(run))
    return render(request, 'form_usuario.html') 