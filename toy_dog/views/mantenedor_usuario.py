from dis import code_info
from logging import exception
from django.shortcuts import render
from toy_dog.models import Usuario

def base_usuario(request):
    print('mantenedorusuario.py -> base_usuario')    
    print('method -> ', request.method)
    if request.method == 'GET':
        try:
            codigo_2 = request.GET['codigo_2']
            print('codigo_2 -> ', codigo_2)
            usuario = Usuario.objects.get(pk = codigo_2)
            usuario.delete()
        except Exception as e:
            print(e)
                 
    if request.method == 'POST':
        try:
            # lectura de formulario
            id = request.POST['codigo_2']
            nombres = request.POST['nombres']
            apellido_paterno = request.POST['apellido-paterno']
            apellido_materno = request.POST['apellido-materno']
            email = request.POST['email']
            telefono = request.POST['telefono']
            
            # busqueda de objeto en la base de dato
            usuario = Usuario.objects.get(pk = id)
            
            #Actualizando en la memoria volatil (memoria ram)
            usuario.nombres = nombres
            usuario.apellido_paterno = apellido_paterno
            usuario.apellido_materno = apellido_materno            
            usuario.email = email
            usuario.telefono = telefono
            
            # Actualiza los datos en la base de datos
            usuario.save(force_update = True)
            
        except Exception as e:
            print(e)   
    usuario = Usuario.objects.all
    return render(request, 'mantenedor-usuario.html', {'usuario': usuario}) 