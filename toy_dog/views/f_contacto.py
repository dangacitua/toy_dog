from django.shortcuts import render
from toy_dog.models import Contacto


def frm_contactos(request):
    print('formulario')
    return render(request, 'formulario.html') 

def env_contacto(request):
    print('formulario_contacto')
    
    if request.method == 'GET':
        print('invocación por método GET')
        run = request.GET.get('run')
        print('run {0}'.format(run))

    elif request.method == 'POST':
        print('invocación por método POST')

        run =                       request.POST.get('run')
        dv =                        request.POST.get('dv')
        nombres =                   request.POST.get('nombres')
        apellido_paterno =          request.POST.get('apellido-paterno')
        apellido_materno =          request.POST.get('apellido-materno')
        email =                     request.POST.get('email')
        telefono =                  request.POST.get('telefono')
        asunto =                    request.POST.get('asunto')  
        #Crear un objeto Contacto que posee relacion con la tabla contacto          
        contacto = Contacto()
        contacto.run =              run
        contacto.dv =               dv
        contacto.nombres =          nombres
        contacto.apellido_paterno = apellido_paterno
        contacto.apellido_materno = apellido_materno
        contacto.email =            email
        contacto.telefono =         telefono
        contacto.asunto =           asunto 
        contacto.save()
         # contacto.save guarda
         # contacto.delete borra
         # contacto.save(force_update)
         # contacto.objects.all muestra todos los datos de la tabla
        print('run {0}'.format(run))

    return render(request, 'formulario.html')

